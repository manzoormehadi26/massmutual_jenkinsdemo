package calculator_test;


import org.junit.Assert;
import org.junit.Test;

import calculator_package.Calculator;

public class Calculator_Test {

	Calculator calc= new Calculator();
	@Test
	public void Add_test() {
		int actualValue=calc.Addition(5, 4);
		int expectedValue=9;
		System.out.println("succesfully executed addition");
		Assert.assertEquals(actualValue, expectedValue);
	}
	
	@Test
	public void Sub_test() {
		int actualValue=calc.Substraction(5, 4);
		int expectedValue=1;
		System.out.println("succesfully executed substraction");
		Assert.assertEquals(actualValue, expectedValue);
	}
}
